package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanKinerja

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import fakta.ayu.pkl_kominfo_android_ekin.KinerjaBaruActivity
import fakta.ayu.pkl_kominfo_android_ekin.R
import kotlinx.android.synthetic.main.fragment_kinerja.*
import kotlinx.android.synthetic.main.fragment_kinerja.view.*
import java.util.*


class KinerjaFragment : Fragment(), View.OnClickListener{

    var tahun = 0
    var bulan = 0
    var tanggal = 0

    private lateinit var kinerjaViewModel: KinerjaViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        kinerjaViewModel =
                ViewModelProviders.of(this).get(KinerjaViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_kinerja, container, false)
        //val textView: TextView = root.findViewById(R.id.txtKinerja)
        kinerjaViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
        })

        val cal: Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)
        tahun = cal.get(Calendar.YEAR)
        tanggal = cal.get(Calendar.DAY_OF_MONTH)

        root.btnBaru.setOnClickListener(this)
        root.btnTAwal.setOnClickListener(this)
        root.btnTAkhir.setOnClickListener(this)
        return root
    }
    override fun onClick(v: View?){
        when (v?.id){
            R.id.btnBaru ->{
                val intent = Intent(getActivity(), KinerjaBaruActivity::class.java)
                startActivity(intent)
            }

            R.id.btnTAwal ->{
                val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    tvFilterAwal.text = "$year-${monthOfYear + 1}-$dayOfMonth"
                    tahun = year
                    bulan = monthOfYear
                    tanggal = dayOfMonth

                }, tahun, bulan, tanggal)
                dpd.show()
            }

                R.id.btnTAkhir ->{
                val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    tvFilterAkhir.text = "$year-${monthOfYear + 1}-$dayOfMonth"
                    tahun = year
                    bulan = monthOfYear
                    tanggal = dayOfMonth

                }, tahun, bulan, tanggal)
                dpd.show()
            }
        }
    }
}