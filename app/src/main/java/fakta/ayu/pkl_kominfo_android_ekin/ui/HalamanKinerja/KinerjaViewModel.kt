package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanKinerja

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class KinerjaViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Kinerja Fragment"
    }
    val text: LiveData<String> = _text
}