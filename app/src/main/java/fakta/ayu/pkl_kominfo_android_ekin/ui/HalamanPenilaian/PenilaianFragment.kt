package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanPenilaian

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import fakta.ayu.pkl_kominfo_android_ekin.*
import kotlinx.android.synthetic.main.activity_barukinerja.*
import kotlinx.android.synthetic.main.custom_button.view.*
import kotlinx.android.synthetic.main.fragment_kinerja.view.*
import kotlinx.android.synthetic.main.fragment_penilaian.*
import kotlinx.android.synthetic.main.fragment_penilaian.view.*
import kotlinx.android.synthetic.main.row_penilaian.view.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.HashMap


class  PenilaianFragment : Fragment(), View.OnClickListener {

//    val urlkinerja = "http://192.168.43.60/ekinerja/show_penilaian.php"
//    val urltahun = "http://192.168.1.101/ekinerja/show_tahun.php"
//    val urlbulan = "http://192.168.1.101/ekinerja/show_bulan.php"
//    val urltahun = "http://192.168.43.228/ekinerja/show_tahun.php"
//    val urlbulan = "http://192.168.43.228/ekinerja/show_bulan.php"

    val urlkinerja = "http://192.168.43.60/ekinerja/show_penilaian.php"
    val urltahun = "http://192.168.43.60/ekinerja/show_tahun.php"
    val urlbulan = "http://192.168.43.60/ekinerja/show_bulan.php"

//    val urlkinerja = "http://192.168.43.228/ekinerja/show_penilaian.php"
//    val urltahun = "http://192.168.43.228/ekinerja/show_tahun.php"
//    val urlbulan = "http://192.168.43.228/ekinerja/show_bulan.php"

//    val urlkinerja = "http://192.168.43.131/e-kinerja-web/show_penilaian.php"
//    val urltahun = "http://192.168.43.131/e-kinerja-web/show_tahun.php"
//    val urlbulan = "http://192.168.43.131/e-kinerja-web/show_bulan.php"

    var _id = ""
    lateinit var session: SharedPref
    lateinit var kinerjaadapter: AdapterPenilaian
    var daftarkinerja = mutableListOf<HashMap<String,String>>()
    private lateinit var layoutManager: LinearLayoutManager

    var tahun = 0
    var bulan = 0
   // var tanggal = 0
    var data = ""
    lateinit var tahunAdapter: ArrayAdapter<String>
    var daftartahun = mutableListOf<String>()
    var pilihTahun = ""
    lateinit var bulanAdapter: ArrayAdapter<String>
    var daftarbulan = mutableListOf<String>()
    var pilihbulan = ""

    val thnpilih = object :AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spTahun.setSelection(0)
            pilihTahun = daftartahun.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihTahun = daftartahun.get(position)
        }
    }

    val blnpilih = object :AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spBulan.setSelection(0)
            pilihbulan = daftarbulan.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihbulan = daftarbulan.get(position)
        }
    }

    private lateinit var penilaianViewModel: PenilaianViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        penilaianViewModel =
                ViewModelProviders.of(this).get(PenilaianViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_penilaian, container, false)
        //val textView: TextView = root.findViewById(R.id.txtPenilaian)
        penilaianViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
        })
  //      root.btnPilih.setOnClickListener(this)
        root.btnTampil.setOnClickListener(this)
        session = SharedPref(context!!)
        _id = SharedPref.getInstance(context!!).user.nip.toString()
        tahunAdapter = ArrayAdapter(
            context!!,android.R.layout.simple_dropdown_item_1line,
            daftartahun
        )
        bulanAdapter = ArrayAdapter(
            context!!,android.R.layout.simple_dropdown_item_1line,
            daftarbulan
        )

        root.spTahun.adapter = tahunAdapter
        root.spTahun.onItemSelectedListener = thnpilih

        root.spBulan.adapter = bulanAdapter
        root.spBulan.onItemSelectedListener = blnpilih

        kinerjaadapter= AdapterPenilaian(daftarkinerja)
        layoutManager = LinearLayoutManager(context!!,RecyclerView.VERTICAL,false)

        root.Listpenilaian.adapter = kinerjaadapter
        root.Listpenilaian.layoutManager = layoutManager
        root.Listpenilaian.addItemDecoration(DividerItemDecoration(root.Listpenilaian.context!!,layoutManager.orientation))

        kinerjaadapter.setOnItemClickListener(object : AdapterPenilaian.OnItemClickListener{
            override fun setOnLongClickListener(v: View) {
                val dv = LayoutInflater.from(context).inflate(R.layout.custom_button,null)
                val build = AlertDialog.Builder(context).setView(dv)
                val dial = build.show()

                dv.idkin.setText(v.txNo.text.toString())

                dv.btn_ubah_menit.setOnClickListener{
                    val intent = Intent(context,EditMenitActivity::class.java)
                    intent.putExtra("id",dv.idkin.text.toString())
                    startActivity(intent)
                    dial.dismiss()
                }
            }
        })

        return root
    }

    override fun onClick(v: View?){
        when (v?.id){
//            R.id.btnPilih ->{
//                val intent = Intent(getActivity(), PilihBawahanActivity::class.java)
//                startActivity(intent)
//            }

//            R.id.PilihTgl ->{
//                val cal: Calendar = Calendar.getInstance()
//                bulan = cal.get(Calendar.MONTH)
//                tahun = cal.get(Calendar.YEAR)
//                tanggal = cal.get(Calendar.DAY_OF_MONTH)
//                val tgl = DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                    txtdBln.setText("$year-${monthOfYear + 1}-$dayOfMonth")
//                    tahun = year
//                    bulan = monthOfYear
//                    tanggal = dayOfMonth
//
//                },tahun,bulan,tanggal)
//                tgl.show()
//            }
            R.id.btnTampil -> {
                if(cbTampilkan.isChecked){
                    show_penilaian(_id,"","", Cari.text.toString())
                }else {
                    show_penilaian(_id, pilihbulan, pilihTahun, Cari.text.toString())
                }
            }

        }
    }
    fun getthn(thn:String){
        val request = object :StringRequest(
            Method.POST,urltahun,
            Response.Listener { response ->
                daftartahun.clear()
                val jsonArray = JSONArray(response)
                for (x in 0.. (jsonArray.length()-1)){
                    val jsonObject= jsonArray.getJSONObject(x)
                    daftartahun.add(jsonObject.getString("tahun"))
                }
                tahunAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(context,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("thn",thn)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(context)
        queue.add(request)
    }

    fun getbln(thn:String){
        val request = object :StringRequest(
            Method.POST,urlbulan,
            Response.Listener { response ->
                daftarbulan.clear()
                val jsonArray = JSONArray(response)
                for (x in 0.. (jsonArray.length()-1)){
                    val jsonObject= jsonArray.getJSONObject(x)
                    daftarbulan.add(jsonObject.getString("bulan"))
                }
                bulanAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(context,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("thn",thn)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(context)
        queue.add(request)
    }

    fun show_penilaian(id_:String,bln:String, thn:String, nama:String){
        val request = object :StringRequest(
            Request.Method.POST,urlkinerja,
            Response.Listener { response ->
                daftarkinerja.clear()
                val jsonArray = JSONArray(response)
                for (x in 0.. (jsonArray.length()-1)){
                    val jsonObject= jsonArray.getJSONObject(x)
                    var kin = HashMap<String,String>()
                    kin.put("id_kinerja",jsonObject.getString("id_kinerja"))
                    kin.put("nama",jsonObject.getString("nama"))
                    kin.put("kategori",jsonObject.getString("kategori"))
                    kin.put("keterangan",jsonObject.getString("keterangan"))
                    kin.put("atasan",jsonObject.getString("atasan"))
                    kin.put("status_kinerja",jsonObject.getString("status_kinerja"))
                    kin.put("menit",jsonObject.getString("menit"))
                    kin.put("jumlah",jsonObject.getString("jumlah"))
                    kin.put("tanggal",jsonObject.getString("tanggal"))
                    daftarkinerja.add(kin)
                }
                kinerjaadapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(context,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("_id",id_)
                hm.put("bln",bln)
                hm.put("thn",thn)
                hm.put("nama",nama)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(context)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        getthn("")
        val han = Handler()
        han.postDelayed({
            if (pilihTahun==""){
            }else{
                getbln(pilihTahun)
            }
        },1000)
    }
}