package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanPenilaian

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PenilaianViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Penilaian Fragment"
    }
    val text: LiveData<String> = _text
}