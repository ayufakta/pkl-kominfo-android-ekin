package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanProfil

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import fakta.ayu.pkl_kominfo_android_ekin.LoginActivity
import fakta.ayu.pkl_kominfo_android_ekin.R
import fakta.ayu.pkl_kominfo_android_ekin.SharedPref
import kotlinx.android.synthetic.main.fragment_profil.*
import org.json.JSONArray
import org.json.JSONObject


class ProfilFragment : Fragment() {

    private lateinit var profilViewModel: ProfilViewModel
    var urlshowprofile = "http://192.168.43.60/ekinerja/show_profil.php"
    var urlatasan = "http://192.168.43.60/ekinerja/atasan.php"
//    var urlshowprofile = "http://192.168.1.101/ekinerja/show_profil.php"
//    var urlatasan = "http://192.168.1.101/ekinerja/atasan.php"
//    var urlshowprofile = "http://192.168.43.228/ekinerja/show_profil.php"
//    var urlatasan = "http://192.168.43.228/ekinerja/atasan.php"
    lateinit var session: SharedPref
//    var urlshowprofile = "http://192.168.43.239/e-kinerja-web/show_profil.php"
//    var urlatasan = "http://192.168.43.239/e-kinerja-web/atasan.php"
//    URL WW
//    var urlshowprofile = "http://192.168.43.131/e-kinerja-web/show_profil.php"
//    var urlatasan = "http://192.168.43.131/e-kinerja-web/atasan.php"
    var _id = ""
    var idatasan = ""
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        profilViewModel =
                ViewModelProviders.of(this).get(ProfilViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profil, container, false)
        profilViewModel.text.observe(viewLifecycleOwner, Observer {
        })

        session = SharedPref(context!!)
        _id = SharedPref.getInstance(context!!).user.nip.toString()
        return root
    }

    override fun onStart() {
        super.onStart()
        ShowProfile(_id)
    }

    fun ShowProfile(id : String){
        val request = object : StringRequest(
            Request.Method.POST,urlshowprofile,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()- 1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prof = HashMap<String, String>()
                    prof.put("nip",jsonObject.getString("nip"))
                    prof.put("nama",jsonObject.getString("nama"))
                    prof.put("golongan",jsonObject.getString("golongan"))
                    prof.put("jabatan",jsonObject.getString("jabatan"))
                    prof.put("eselon",jsonObject.getString("eselon"))
                    prof.put("unit_kerja",jsonObject.getString("unit_kerja"))
                    prof.put("id_atasan",jsonObject.getString("id_atasan"))

                    textView57.setText(prof.getValue("nip").toString())
                    textView58.setText(prof.getValue("nama").toString())
                    textView56.setText(prof.getValue("golongan").toString())
                    textView62.setText(prof.getValue("jabatan").toString())
                    textView63.setText(prof.getValue("eselon").toString())
                    textView64.setText(prof.getValue("unit_kerja").toString())
                    idatasan = prof.getValue("id_atasan").toString()
                    atasan(idatasan)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(context, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String,String>()
                hm.put("_id",id)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(context)
        queue.add(request)
    }

    fun atasan(id : String){
        val request = object : StringRequest(
            Request.Method.POST,urlatasan,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()- 1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prof = HashMap<String, String>()
                    prof.put("nama",jsonObject.getString("nama"))
                    prof.put("jabatan",jsonObject.getString("jabatan"))

                    textView60.setText(prof.getValue("nama").toString())
                    textView65.setText(prof.getValue("jabatan").toString())
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(context, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String,String>()
                hm.put("_id",id)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(context)
        queue.add(request)
    }
}