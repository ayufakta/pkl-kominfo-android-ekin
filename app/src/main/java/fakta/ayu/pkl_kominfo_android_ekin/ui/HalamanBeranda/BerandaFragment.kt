package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanBeranda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import fakta.ayu.pkl_kominfo_android_ekin.R


class BerandaFragment : Fragment(), View.OnClickListener {

    private lateinit var berandaViewModel: BerandaViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        berandaViewModel =
                ViewModelProviders.of(this).get(BerandaViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_beranda, container, false)
        //val textView: TextView = root.findViewById(R.id.text_beranda)
        berandaViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
        })
        return root
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

}