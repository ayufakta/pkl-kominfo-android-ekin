package fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanPenilaian

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fakta.ayu.pkl_kominfo_android_ekin.R

class AdapterPenilaian(val data : List<HashMap<String,String>>) :
    RecyclerView.Adapter<AdapterPenilaian.HolderData>(){

    private lateinit var mListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPenilaian.HolderData {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_penilaian,parent,false)
        return HolderData(v,mListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: AdapterPenilaian.HolderData, position: Int) {
        val dat = data.get(position)
        holder.txNo.setText(dat.getValue("id_kinerja"))
        holder.txNama.setText(dat.getValue("nama"))
        holder.txKegiatan.setText(dat.getValue("kategori"))
        holder.txKeterangan.setText(dat.getValue("keterangan"))
        holder.txPenilai.setText(dat.getValue("atasan"))
        holder.txStatus.setText(dat.getValue("status_kinerja"))
        holder.txTanggal.setText(dat.getValue("tanggal"))
        holder.txMenit.setText(dat.getValue("menit"))
        holder.txKuantitas.setText(dat.getValue("jumlah"))
    }

    class HolderData(v : View, var mListener: OnItemClickListener): RecyclerView.ViewHolder(v), View.OnClickListener,
            View.OnLongClickListener{
        override fun onClick(v: View?) {

        }
        val txNo = v.findViewById<TextView>(R.id.txNo)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txKegiatan = v.findViewById<TextView>(R.id.txKegiatan)
        val txKeterangan = v.findViewById<TextView>(R.id.txKeterangan)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val txPenilai = v.findViewById<TextView>(R.id.txPenilai)
        val txStatus = v.findViewById<TextView>(R.id.txStatus)
        val txMenit = v.findViewById<TextView>(R.id.txMenit)
        val txKuantitas = v.findViewById<TextView>(R.id.txKuantitas)

        init {
            itemView.setOnLongClickListener(this)
        }

        override fun onLongClick(p0: View):Boolean{
            if (mListener != null){
                mListener.setOnLongClickListener(p0)
            }
            return true
        }
    }
    interface OnItemClickListener
    {
        fun setOnLongClickListener(v:View)
    }

    fun setOnItemClickListener(mListener: OnItemClickListener){
        this.mListener = mListener
    }
}