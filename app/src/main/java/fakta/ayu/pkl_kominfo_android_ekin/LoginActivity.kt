package fakta.ayu.pkl_kominfo_android_ekin

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.EditText
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class LoginActivity : AppCompatActivity() {

    lateinit var etNip: EditText
    internal lateinit var etPassword: EditText
//    val urlLogin = "http://192.168.1.101/ekinerja/login.php?apicall=login"
//    val urlLogin = "http://192.168.43.228/ekinerja/login.php?apicall=login"

//    val urlLogin = "http://192.168.43.239/e-kinerja-web/login.php?apicall=login"
//    URL WW
//    val urlLogin = "http://192.168.43.131/e-kinerja-web/login.php?apicall=login"
    val urlLogin = "http://192.168.43.60/ekinerja/login.php?apicall=login"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        if (SharedPref.getInstance(this).isLoggedIn) {
            if (SharedPref.getInstance(this).user.id_akses == "3") {
                finish()
                startActivity(Intent(this, MenuUserActivity::class.java))
            } else if (SharedPref.getInstance(this).user.id_akses == "4") {
                finish()
                startActivity(Intent(this, MenuUserActivity::class.java))
            } else {
            }
        }
        etNip = findViewById(R.id.user)
        etPassword = findViewById(R.id.pass)

        //calling the method userLogin() for login the user
        btnlogin.setOnClickListener(View.OnClickListener {
            userLogin()
        })

    }

    private fun userLogin() {
        //first getting the values
        val nip = etNip.text.toString()
        val password = etPassword.text.toString()
        //validating inputs
        if (TextUtils.isEmpty(nip)) {
            etNip.error = "Please enter your username"
            etNip.requestFocus()
            return
        }

        if (TextUtils.isEmpty(password)) {
            etPassword.error = "Please enter your password"
            etPassword.requestFocus()
            return
        }

        //if everything is fine
        val stringRequest = object : StringRequest(Request.Method.POST, urlLogin,
            Response.Listener { response ->


                try {
                    //converting response to json object
                    val obj = JSONObject(response)
                    val mes = obj.getString("message")

                    if (mes!=""){
                        Toast.makeText(applicationContext, mes, Toast.LENGTH_SHORT).show()
                    }else{
                    }
                    //if no error in response
                    if (!obj.getBoolean("error")) {


                        //getting the user from the response
                        val userJson = obj.getJSONObject("user")

                        //creating a new user object
                        val user = User(
                            userJson.getString("id_akses"),
                            userJson.getString("nip"),
                            userJson.getString("password")

                        )
                        //storing the user in shared preferences
                        SharedPref.getInstance(applicationContext).userLogin(user)
                        if(user.id_akses.toString() == "3"){
                            finish()
                            startActivity(Intent(applicationContext, MenuUserActivity::class.java))
                        }else if(user.id_akses.toString()=="4"){
                            finish()
                            startActivity(Intent(applicationContext, MenuUserActivity::class.java))
                        }

                    } else {
                        Toast.makeText(
                            applicationContext,
                            obj.getString("message"),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    applicationContext,
                    error.message,
                    Toast.LENGTH_SHORT
                ).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["nip"] = nip
                params["password"] = password
                return params
            }
        }

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        moveTaskToBack(true)
    }
}



    //val url = "http://192.168.1.101/ekinerja/login.php"
    //val url = "http://192.168.43.60/ekinerja/login.php"
    //val url = "http://192.168.43.228/ekinerja/login.php"
//    val RC_LOGOUT_SUKSES : Int = 10
//    val RC_LOGIN_SUKSES : Int = 5
//    var txuser = ""
//    var txpass = ""
//    var pimpinan = "6"
//    var admin = "6"
//    var id = "0"
//    lateinit var SETTING : SharedPreferences
//    val SNAMA = "Setting"
//    val SID = "Id"
//    val SPIM = "Pim"
//    val SADMIN = "Admin"
//    val SUSER = "User"
//    val SPASS = "Pass"
//    var dpim = "6"
//    var dpass = ""
//    var duser = ""
//    var did = "0"
//    var dadmin = "6"
//    val IID = did
//    val IPIM = dpim
//    val IADMIN = dadmin
//    val IUSER = duser
//    val IPASS = dpass

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login)
//        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
//        duser = SETTING.getString(SUSER,IUSER).toString()
//        dpass = SETTING.getString(SPASS,IPASS).toString()
//        dpim = SETTING.getString(SPIM,IPIM).toString()
//        dadmin = SETTING.getString(SADMIN,IADMIN).toString()
//        did = SETTING.getString(SID,IID).toString()
//        cek()
//        btnlogin.setOnClickListener(this)
//
//
//    }

//    override fun onClick(v: View?) {
//        when (v?.id) {
//            R.id.btnlogin -> {
//                txuser = user.text.toString()
//                txpass = pass.text.toString()
//                halaman("login")
//                bagian()
//                //Toast.makeText(this,"$txuser, $txpass",Toast.LENGTH_LONG).show()
//                //var intent = Intent(this, MenuUserActivity::class.java)
//                //startActivity(intent)
//            }
//        }
//    }

//    fun halaman(mode : String){
//        val request = object : StringRequest(
//            Request.Method.POST,url,
//            Response.Listener { response ->
//                //Toast.makeText(this,"mulai login",Toast.LENGTH_SHORT).show()
//                val jsonObject = JSONObject(response)
//                pimpinan = jsonObject.getString("pimpinan")
//                admin = jsonObject.getString("admin")
//                id = jsonObject.getString("id")
//                dpim = pimpinan
//                dadmin = admin
//                did = id
//            },
//            Response.ErrorListener { error ->
//                Toast.makeText(this, "gagal melakukan login",Toast.LENGTH_SHORT).show()
//            }
//        ){
//            override fun getParams(): MutableMap<String, String> {
//
//                val hm = HashMap<String,String>()
//                when(mode){
//                    "login"->{
//                        hm.put("txdatauser",txuser)
//                        hm.put("txdatapass", txpass)
//                    }
//                }
//            return hm
//            }
//        }
//        val queue = Volley.newRequestQueue(this)
//        queue.add(request)
//
//    }

//    fun bagian(){
//        if (did=="0"){
//            Toast.makeText(this, "silakan login untuk menggunakan aplikasi",Toast.LENGTH_SHORT).show()
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="0"){
//            val AkunUser = Intent(this,MenuUserActivity::class.java)
//            AkunUser.putExtra("id",did)
//            simpan()
//            startActivityForResult(AkunUser,RC_LOGOUT_SUKSES)
//            //Toast.makeText(this, "melakukan login $id,$admin,$pimpinan",Toast.LENGTH_SHORT).show()
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="1"){
//            val AkunPimpinan = Intent(this,MenuUserActivity::class.java)
//            AkunPimpinan.putExtra("id",did)
//            simpan()
//            startActivityForResult(AkunPimpinan,RC_LOGOUT_SUKSES)
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="9"){
////            val AkunPimpinan = Intent(this,MenuUserActivity::class.java)
////            AkunPimpinan.putExtra("id",did)
////            startActivityForResult(AkunPimpinan,RC_LOGOUT_SUKSES)
//            Toast.makeText(this, "Akun sudah terdaftar sebagai admin",Toast.LENGTH_SHORT).show()
//        }
//    }

//    fun cek(){
//        if (did=="0"){
//            Toast.makeText(this, "silakan login untuk menggunakan aplikasi",Toast.LENGTH_SHORT).show()
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="0"){
//            val AkunUser = Intent(this,MenuUserActivity::class.java)
//            AkunUser.putExtra("id",did)
//            startActivityForResult(AkunUser,RC_LOGOUT_SUKSES)
//
//            //Toast.makeText(this, "melakukan login $id,$admin,$pimpinan",Toast.LENGTH_SHORT).show()
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="1"){
//            val AkunPimpinan = Intent(this,MenuUserActivity::class.java)
//            AkunPimpinan.putExtra("id",did)
//            startActivityForResult(AkunPimpinan,RC_LOGOUT_SUKSES)
//        }
//        else if (did!="0"&&dpim=="0"&&dadmin=="9"){
////            val AkunPimpinan = Intent(this,MenuUserActivity::class.java)
////            AkunPimpinan.putExtra("id",did)
////            startActivityForResult(AkunPimpinan,RC_LOGOUT_SUKSES)
//            Toast.makeText(this, "Akun sudah terdaftar sebagai admin",Toast.LENGTH_SHORT).show()
//        }
//
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == Activity.RESULT_OK){
//            if (requestCode == RC_LOGIN_SUKSES){
//                did=data?.extras?.getString("A").toString()
//                dpim=data?.extras?.getString("B").toString()
//                dadmin=data?.extras?.getString("C").toString()
//                duser=data?.extras?.getString("D").toString()
//                dpass=data?.extras?.getString("E").toString()
//                simpan()
//                bagian()
//            }
//            if (requestCode == RC_LOGOUT_SUKSES){
//                did=""
//                dpim=""
//                dadmin=""
//                duser=""
//                dpass=""
//                simpan()
//            }
//        }
//    }

//    fun simpan(){
//        duser = txuser
//        dpass = txpass
//        Toast.makeText(this, "DATA $duser, $dpass, $did, $dadmin, $dpim DISIMPAN",Toast.LENGTH_SHORT).show()
//        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
//        val SEDIT = SETTING.edit()
//        SEDIT.putString(SID,did)
//        SEDIT.putString(SADMIN,dadmin)
//        SEDIT.putString(SPIM,dpim)
//        SEDIT.putString(SUSER,duser)
//        SEDIT.putString(SPASS,dpass)
//        SEDIT.commit()
//    }

//    fun IDUSer(){
//        Toast.makeText(this,"isi dari ak8n $duser,$dpim,$did,$dadmin,$dpass",Toast.LENGTH_SHORT).show()
//        val AkunUser = Intent(this,MenuUserActivity::class.java)
//        AkunUser.putExtra("id",id)
//        startActivityForResult(AkunUser,RC_LOGOUT_SUKSES)
//    }
//    fun IDPimpinan(){
//        Toast.makeText(this,"isi dari ak8n $duser,$dpim,$did,$dadmin,$dpass",Toast.LENGTH_SHORT).show()
//        val AkunPimpinan = Intent(this,MenuUserActivity::class.java)
//        AkunPimpinan.putExtra("id",id)
//        startActivityForResult(AkunPimpinan,RC_LOGOUT_SUKSES)
//    }

    //nyoba logout
    //override fun onCreateContextMenu(menu: ContextMenu?, v:View?, menuInfo: ContextMenu.ContextMenuInfo?) {
      //  var mnuInflater = getMenuInflater()
        //mnuInflater.inflate(R.menu.user_logout, menu)
    //}

     //override fun onContextItemSelected(item: MenuItem): Boolean {
       // when(item?.itemId){
         //   R.id.user_logout ->{
           //     Toast.makeText(this, "melakukan logout",Toast.LENGTH_SHORT).show()
             //   return true
            //}
        //}
        //return super.onContextItemSelected(item)
    //}

    //}

