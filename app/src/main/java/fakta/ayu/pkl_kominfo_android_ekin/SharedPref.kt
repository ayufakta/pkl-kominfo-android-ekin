package fakta.ayu.pkl_kominfo_android_ekin

import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Context
import android.content.Intent

class SharedPref constructor(context: Context){

    //this method will checker whether user is already logged in or not
    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = ctx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences?.getString(KEY_NIP, null) != null
        }

    //this method will give the logged in user
    val user: User
        get() {
            val sharedPreferences = ctx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return User(
                sharedPreferences!!.getString(KEY_ID_AKSES, null),
                sharedPreferences.getString(KEY_NIP, null),
                sharedPreferences.getString(KEY_PASSWORD, null)

            )
        }

    init {
        ctx = context
    }

    //this method will store the user data in shared preferences
    fun userLogin(user: User) {
        val sharedPreferences = ctx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString(KEY_ID_AKSES, user.id_akses)
        editor?.putString(KEY_NIP, user.nip)
        editor?.putString(KEY_PASSWORD, user.password)
        editor?.apply()
    }

    //this method will logout the user
    fun logout() {
        val sharedPreferences = ctx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.clear()
        editor?.apply()

        var i: Intent = Intent(ctx,LoginActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        ctx?.startActivity(i)

    }

    companion object {

        private val SHARED_PREF_NAME = "volleyregisterlogin"
        private val KEY_NIP = "keynip"
        private val KEY_PASSWORD = "keypassword"
        private val KEY_ID_AKSES = "keyidakses"
        private var mInstance: SharedPref? = null
        private var ctx: Context? = null
        @Synchronized
        fun getInstance(context: Context): SharedPref {
            if (mInstance == null) {
                mInstance = SharedPref(context)
            }
            return mInstance as SharedPref
        }
    }
}