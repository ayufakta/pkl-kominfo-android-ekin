package fakta.ayu.pkl_kominfo_android_ekin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import fakta.ayu.pkl_kominfo_android_ekin.ui.HalamanPenilaian.PenilaianFragment
import kotlinx.android.synthetic.main.activity_editmenit.*
import org.json.JSONArray
import org.json.JSONObject

class EditMenitActivity :AppCompatActivity(){

    var _id=""
    var urlkinerja = "http://192.168.43.60/ekinerja/show_kinerja.php"
    var urlubahmenit = "http://192.168.43.60/ekinerja/ubahmenit.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editmenit)

        _id = intent.getStringExtra("id").toString()

        btnBatal.setOnClickListener {
            onBackPressed()
        }

        btnSimpan.setOnClickListener {
            ubahmenit("update",_id,WaktuAwal.text.toString(),WaktuAkhir.text.toString())
            val han = Handler()
            val intent = Intent(this,MenuUserActivity::class.java)
            han.postDelayed({
                startActivity(intent)
            },500)


        }
    }

    override fun onStart() {
        super.onStart()
        getkinerja(_id)
    }
    fun getkinerja(idk:String){
        val request = object : StringRequest(
            Method.POST,urlkinerja,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0.. (jsonArray.length()-1)){
                    val jsonObject= jsonArray.getJSONObject(x)
                    var ed = HashMap<String,String>()
                    ed.put("kategori",jsonObject.getString("kategori"))
                    ed.put("keterangan",jsonObject.getString("keterangan"))
                    ed.put("catatan",jsonObject.getString("catatan"))
                    ed.put("output",jsonObject.getString("output"))
                    ed.put("jumlah",jsonObject.getString("jumlah"))
                    ed.put("waktu_awal",jsonObject.getString("waktu_awal"))
                    ed.put("waktu_akhir",jsonObject.getString("waktu_akhir"))
                    ed.put("tanggal",jsonObject.getString("tanggal"))

                    NmKinerja.setText(ed.getValue("kategori").toString())
                    Keterangan.setText(ed.getValue("keterangan").toString())
                    Catatan.setText(ed.getValue("catatan").toString())
                    Output.setText(ed.getValue("output").toString())
                    Jumlah.setText(ed.getValue("jumlah").toString())
                    WaktuAwal.setText(ed.getValue("waktu_awal").toString())
                    WaktuAkhir.setText(ed.getValue("waktu_akhir").toString())
                    Tanggal.setText(ed.getValue("tanggal").toString())
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("idk",idk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun ubahmenit(mode:String, id_:String, awal:String, akhir:String){
        val Request = object : StringRequest(Method.POST,urlubahmenit,
        Response.Listener { response ->
            val jsonObject = JSONObject(response)
            val error = jsonObject.getString("kode")
            if (error.equals("000")) {
                Toast.makeText(this, "Ubah Menit Berhasil", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Ubah Menit Gagal", Toast.LENGTH_LONG).show()
            }
        },
        Response.ErrorListener { error ->
            Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
        }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id",id_)
                        hm.put("awal",awal)
                        hm.put("akhir",akhir)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(Request)
    }

}