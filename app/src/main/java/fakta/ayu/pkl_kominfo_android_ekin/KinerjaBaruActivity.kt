package fakta.ayu.pkl_kominfo_android_ekin

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.icu.text.IDNA
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_barukinerja.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class KinerjaBaruActivity: AppCompatActivity() {
//    val urlgetunitkerja = "http://192.168.43.131/e-kinerja-web/get_unitkerja.php"
//    val urlgetbidangkerja = "http://192.168.43.131/e-kinerja-web/get_bidangkerja.php"
//    val urlgetseksikerja = "http://192.168.43.131/e-kinerja-web/get_seksikerja.php"
//    val urlgetjeniskinerja = "http://192.168.43.131/e-kinerja-web/get_jeniskinerja.php"
//    val urlgetkategori = "http://192.168.43.131/e-kinerja-web/get_kategori.php"
//    val urlgetoutput = "http://192.168.43.131/e-kinerja-web/get_output.php"
//    val urlinsert = "http://192.168.43.131/e-kinerja-web/insertkinerja.php"

//    val urlgetunitkerja = "http://192.168.43.239/e-kinerja-web/get_unitkerja.php"
//    val urlgetbidangkerja = "http://192.168.43.239/e-kinerja-web/get_bidangkerja.php"
//    val urlgetseksikerja = "http://192.168.43.239/e-kinerja-web/get_seksikerja.php"
//    val urlgetjeniskinerja = "http://192.168.43.239/e-kinerja-web/get_jeniskinerja.php"
//    val urlgetkategori = "http://192.168.43.239/e-kinerja-web/get_kategori.php"
//    val urlgetoutput = "http://192.168.43.239/e-kinerja-web/get_output.php"
//    val urlinsert = "http://192.168.43.239/e-kinerja-web/insertkinerja.php"

//    val urlgetunitkerja = "http://192.168.43.228/ekinerja/get_unitkerja.php"
//    val urlgetbidangkerja = "http://192.168.43.228/ekinerja/get_bidangkerja.php"
//    val urlgetseksikerja = "http://192.168.43.228/ekinerja/get_seksikerja.php"
//    val urlgetjeniskinerja = "http://192.168.43.228/ekinerja/get_jeniskinerja.php"
//    val urlgetkategori = "http://192.168.43.228/ekinerja/get_kategori.php"
//    val urlgetoutput = "http://192.168.43.228/ekinerja/get_output.php"
//    val urlinsert = "http://192.168.43.228/ekinerja/insertkinerja.php"

        val urlgetunitkerja = "http://192.168.43.60/ekinerja/get_unitkerja.php"
        val urlgetbidangkerja = "http://192.168.43.60/ekinerja/get_bidangkerja.php"
        val urlgetseksikerja = "http://192.168.43.60/ekinerja/get_seksikerja.php"
        val urlgetjeniskinerja = "http://192.168.43.60/ekinerja/get_jeniskinerja.php"
        val urlgetkategori = "http://192.168.43.60/ekinerja/get_kategori.php"
        val urlgetoutput = "http://192.168.43.60/ekinerja/get_output.php"
        val urlinsert = "http://192.168.43.60/ekinerja/insertkinerja.php"

//    val urlgetunitkerja = "http://192.168.1.101/ekinerja/get_unitkerja.php"
//    val urlgetbidangkerja = "http://192.168.1.101/ekinerja/get_bidangkerja.php"
//    val urlgetseksikerja = "http://192.168.1.101/ekinerja/get_seksikerja.php"
//    val urlgetjeniskinerja = "http://192.168.1.101/ekinerja/get_jeniskinerja.php"
//    val urlgetkategori = "http://192.168.1.101/ekinerja/get_kategori.php"
//    val urlgetoutput = "http://192.168.1.101/ekinerja/get_output.php"
//    val urlinsert = "http://192.168.1.101/ekinerja/insertkinerja.php"


    lateinit var session: SharedPref
    lateinit var bidangkerjaAdapter: ArrayAdapter<String>
    lateinit var seksikerjaAdapter: ArrayAdapter<String>
    lateinit var jeniskinerjaAdapter: ArrayAdapter<String>
    lateinit var kategoriAdapter: ArrayAdapter<String>
    lateinit var outputAdapter: ArrayAdapter<String>

    var daftarBidangKerja = mutableListOf<String>()
    var daftarIdBidangKerja = mutableListOf<String>()

    var daftarKategori = mutableListOf<String>()
    var daftarIdKategori = mutableListOf<String>()

    var daftarSeksiKerja = mutableListOf<String>()
    var daftarIdSeksiKerja = mutableListOf<String>()

    var daftarJenisKinerja = mutableListOf<String>()
    var daftarIdJenisKinerja = mutableListOf<String>()

    var daftarOutput = mutableListOf<String>()
    var daftarIdOutput = mutableListOf<String>()

    var pilihOutput = ""
    var pilihIdOutput = ""

    var pilihBidangKerja = ""
    var pilihIdBidangKerja = ""

    var pilihKategori = ""
    var pilihIdKategori = ""

    var pilihSeksiKerja = ""
    var pilihIdSeksiKerja = ""

    var pilihJenisKinerja = ""
    var pilihIdJenisKinerja = ""

    var jamawal = 0
    var menitawal = 0

    var jamakhir = 0
    var menitakhir = 0

    var tahun = 0
    var bulan = 0
    var tanggal = 0

    var _id = ""
    var id_unit_kerja = ""
    var id_bidang_kerja = ""
    var id_seksi_kerja = ""

    var id_jeniskinerja = ""
    var id_seksi = ""
    var id_kategori = ""
    var id_output = ""
    var id_bidang = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView((R.layout.activity_barukinerja))

//        bidangkerjaAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarBidangKerja)
//        spinBidangKerja.adapter = bidangkerjaAdapter
//        spinBidangKerja.onItemSelectedListener = itemSelected
//
//        seksikerjaAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarSeksiKerja)
//        spinSeksiKerja.adapter = seksikerjaAdapter
//        spinSeksiKerja.onItemSelectedListener = itemSelectedSeksi

        jeniskinerjaAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarJenisKinerja)
        spinJenisKinerja.adapter = jeniskinerjaAdapter
        spinJenisKinerja.onItemSelectedListener = itemSelectedJenisKinerja

        kategoriAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarKategori)
        spinKategori.adapter = kategoriAdapter
        spinKategori.onItemSelectedListener = itemSelectedKategori

        outputAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarOutput)
        spinOutput.adapter = outputAdapter
        spinOutput.onItemSelectedListener = itemSelectedOutput

        val cal: Calendar = Calendar.getInstance()

        bulan = cal.get(Calendar.MONTH)
        tahun = cal.get(Calendar.YEAR)
        tanggal = cal.get(Calendar.DAY_OF_MONTH)

        btnDpAdd.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in Toast
                ettDateAdd.text = "$year-${monthOfYear + 1}-$dayOfMonth"
                tahun = year
                bulan = monthOfYear
                tanggal = dayOfMonth

            }, tahun, bulan, tanggal)
            dpd.show()
        }

        btnTAwal.setOnClickListener {
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                // Display Selected date in Toast
                ettAwal.text = String.format("%02d:%02d", hourOfDay, minute)

            }, jamawal, menitawal, true)
            tpd.show()
        }

        btnTAkhir.setOnClickListener {
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                // Display Selected date in Toast
                ettAkhir.text = String.format("%02d:%02d", hourOfDay, minute)

            }, jamakhir, menitakhir, true)
            tpd.show()
        }

        btnSbmKin.setOnClickListener{
            queryInsert("insert")
        }

        session = SharedPref(this)
        _id = SharedPref.getInstance(this).user.nip.toString()
    }

    override fun onStart() {
        super.onStart()
        getUnitKerja(_id)
        getOutput()
    }

    //  Spinner Bidang
//    val itemSelected = object : AdapterView.OnItemSelectedListener{
//        override fun onNothingSelected(parent: AdapterView<*>?) {
//            spinBidangKerja.setSelection(0)
//            pilihBidangKerja = daftarBidangKerja.get(0)
//
//        }
//
//        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//            pilihBidangKerja = daftarBidangKerja.get(position)
//            pilihIdBidangKerja = daftarIdBidangKerja.get(position)
//            id_bidang = pilihIdBidangKerja.toString()
//            var text = pilihBidangKerja.toString()
////            Log.println(Log.INFO, "LOG", id)
////            Log.println(Log.INFO, "LOG", text)
//
//            if (id_bidang !== "0"){
//                getSeksiKerja(id_bidang)
//            }
//        }
//    }

    //    Spinner Output
    val itemSelectedOutput = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinOutput.setSelection(0)
            pilihOutput = daftarOutput.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihOutput = daftarOutput.get(position)
            pilihIdOutput = daftarIdOutput.get(position)
            var text = pilihOutput.toString()
            id_output = pilihIdOutput.toString()
//            Log.println(Log.INFO, "LOG", id)
//            Log.println(Log.INFO, "LOG", text)
        }
    }

    //    Spinner Kategori
    val itemSelectedKategori = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
            pilihIdKategori = daftarIdKategori.get(position)
            id_kategori = pilihIdKategori.toString()
            var text = pilihKategori.toString()
//            Log.println(Log.INFO, "LOG", id)
//            Log.println(Log.INFO, "LOG", text)
        }
    }

    //  Spinner Seksi
//    val itemSelectedSeksi = object : AdapterView.OnItemSelectedListener{
//        override fun onNothingSelected(parent: AdapterView<*>?) {
//            spinSeksiKerja.setSelection(0)
//            pilihSeksiKerja = daftarSeksiKerja.get(0)
//        }
//
//        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//            pilihSeksiKerja = daftarSeksiKerja.get(position)
//            pilihIdSeksiKerja = daftarIdSeksiKerja.get(position)
//            id_seksi = pilihIdSeksiKerja.toString()
//            var text = pilihSeksiKerja.toString()
////            Log.println(Log.INFO, "LOG", id)
////            Log.println(Log.INFO, "LOG", text)
//
//            if (id_seksi !== "0"){
//                getJenisKinerja(id_seksi)
//            }
//        }
//    }

    //    Spinner Jenis Kinerja
    val itemSelectedJenisKinerja = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinJenisKinerja.setSelection(0)
            pilihJenisKinerja = daftarJenisKinerja.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJenisKinerja = daftarJenisKinerja.get(position)
            pilihIdJenisKinerja = daftarIdJenisKinerja.get(position)
            id_jeniskinerja = pilihIdJenisKinerja.toString()
            var text = pilihJenisKinerja.toString()
//            Log.println(Log.INFO, "LOG", id)
//            Log.println(Log.INFO, "LOG", text)
            if (id_jeniskinerja !== "0"){
                getKategori(id_jeniskinerja)
            }
        }
    }

    fun queryInsert(mode : String){
        val request = object : StringRequest(
            Method.POST,urlinsert,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this, "Sukses Menambahkan kinerja",Toast.LENGTH_LONG).show()
                    finish()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = java.util.HashMap<String, String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_jeniskinerja",id_jeniskinerja)
                        hm.put("id_output", id_output)
                        hm.put("nip", _id)
                        hm.put("id_kategori", id_kategori)
                        hm.put("keterangan", txiKet.text.toString())
                        hm.put("catatan", txiCat.text.toString())
                        hm.put("jumlah", txiJml.text.toString())
                        hm.put("tanggal", ettDateAdd.text.toString())
                        hm.put("waktu_awal", ettAwal.text.toString())
                        hm.put("waktu_akhir", ettAkhir.text.toString())
                        hm.put("id_status_kinerja", "4")
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getOutput(){
        val request = StringRequest(
            Request.Method.POST,urlgetoutput,
            Response.Listener { response ->
                daftarOutput.clear()
                daftarIdOutput.clear()
                daftarOutput.add("--- Pilih Output ---")
                daftarIdOutput.add("0")
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarOutput.add(jsonObject.getString("output"))
                    daftarIdOutput.add(jsonObject.getString("id_output"))
                }
                outputAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getKategori(id_jeniskinerja :String){
        val request = object : StringRequest(
            Method.POST,urlgetkategori,
            Response.Listener { response ->
                daftarKategori.clear()
                daftarIdKategori.clear()
                daftarKategori.add("--- Pilih Kategori ---")
                daftarIdKategori.add("0")
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()- 1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("kategori"))
                    daftarIdKategori.add(jsonObject.getString("id_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String,String>()
                hm.put("id_jeniskinerja",id_jeniskinerja)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getJenisKinerja(id_seksi :String){
        val request = object : StringRequest(
            Method.POST,urlgetjeniskinerja,
            Response.Listener { response ->
                daftarJenisKinerja.clear()
                daftarIdJenisKinerja.clear()
                daftarJenisKinerja.add("--- Pilih Jenis Kinerja ---")
                daftarIdJenisKinerja.add("0")
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()- 1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarJenisKinerja.add(jsonObject.getString("jenis_kinerja"))
                    daftarIdJenisKinerja.add(jsonObject.getString("id_jeniskinerja"))
                }
                jeniskinerjaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String,String>()
                hm.put("id_seksi",id_seksi)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

//    fun getSeksiKerja(id_bidangkerja : String){
//        val request = object : StringRequest(
//            Method.POST,urlgetseksikerja,
//            Response.Listener { response ->
//                daftarSeksiKerja.clear()
//                daftarIdSeksiKerja.clear()
//                daftarSeksiKerja.add("--- Pilih Seksi Kerja ---")
//                daftarIdSeksiKerja.add("0")
//                val jsonArray = JSONArray(response)
//                for(x in 0..(jsonArray.length()- 1)){
//                    val jsonObject = jsonArray.getJSONObject(x)
//                    daftarSeksiKerja.add(jsonObject.getString("seksi_kerja"))
//                    daftarIdSeksiKerja.add(jsonObject.getString("id_seksi_kerja"))
//                }
//                seksikerjaAdapter.notifyDataSetChanged()
//            },
//            Response.ErrorListener { error ->
//                Toast.makeText(this, "gagal terhubung", Toast.LENGTH_SHORT).show()
//            }
//        ){
//            override fun getParams(): MutableMap<String, String> {
//
//                val hm = HashMap<String,String>()
//                hm.put("id_bidangkerja",id_bidangkerja)
//                return hm
//            }
//        }
//        val queue = Volley.newRequestQueue(this)
//        queue.add(request)
//    }

//    fun getBidangKerja(id_unit_kerja : String){
//        val request = object : StringRequest(
//            Method.POST,urlgetbidangkerja,
//            Response.Listener { response ->
//                daftarBidangKerja.clear()
//                daftarIdBidangKerja.clear()
//                daftarBidangKerja.add("--- Pilih Bidang Kerja ---")
//                daftarIdBidangKerja.add("0")
//                val jsonArray = JSONArray(response)
//                for(x in 0..(jsonArray.length()- 1)){
//                    val jsonObject = jsonArray.getJSONObject(x)
//                    daftarBidangKerja.add(jsonObject.getString("bidang_kerja"))
//                    daftarIdBidangKerja.add(jsonObject.getString("id_bidang_kerja"))
//                }
//                bidangkerjaAdapter.notifyDataSetChanged()
//            },
//            Response.ErrorListener { error ->
//                Toast.makeText(this, "gagal terhubung", Toast.LENGTH_SHORT).show()
//            }
//        ){
//            override fun getParams(): MutableMap<String, String> {
//
//                val hm = HashMap<String,String>()
//                hm.put("id_unit_kerja",id_unit_kerja)
//                return hm
//            }
//        }
//        val queue = Volley.newRequestQueue(this)
//        queue.add(request)
//    }

    fun getUnitKerja(id : String){
        val request = object : StringRequest(
            Method.POST,urlgetunitkerja,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()- 1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prof = HashMap<String, String>()
                    prof.put("unit_kerja",jsonObject.getString("unit_kerja"))
                    prof.put("id_unit_kerja",jsonObject.getString("id_unit_kerja"))
                    prof.put("bidang_kerja",jsonObject.getString("bidang_kerja"))
                    prof.put("id_bidang_kerja",jsonObject.getString("id_bidang_kerja"))
                    prof.put("seksi_kerja",jsonObject.getString("seksi_kerja"))
                    prof.put("id_seksi_kerja",jsonObject.getString("id_seksi_kerja"))

                    tv_unit.setText(prof.getValue("unit_kerja").toString())
                    tv_bidangkerja.setText(prof.getValue("bidang_kerja").toString())
                    tv_seksikerja.setText(prof.getValue("seksi_kerja").toString())
                    id_unit_kerja = prof.getValue("id_unit_kerja").toString()
                    id_bidang_kerja = prof.getValue("id_bidang_kerja").toString()
                    id_seksi_kerja = prof.getValue("id_seksi_kerja").toString()
                    getJenisKinerja(id_seksi_kerja)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String,String>()
                hm.put("_id",id)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}