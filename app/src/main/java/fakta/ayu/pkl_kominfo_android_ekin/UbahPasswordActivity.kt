package fakta.ayu.pkl_kominfo_android_ekin

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_ubahpw.*
import kotlinx.android.synthetic.main.fragment_profil.*
import org.json.JSONArray
import org.json.JSONObject

class UbahPasswordActivity: AppCompatActivity(), View.OnClickListener {

    var urlshowprofile = "http://192.168.43.60/ekinerja/show_profil.php"
    var urlatasan = "http://192.168.43.60/ekinerja/atasan.php"
    var gantipw = "http://192.168.43.60/ekinerja/gantipw.php"

//    var urlshowprofile = "http://192.168.1.101/ekinerja/show_profil.php"
//    var urlatasan = "http://192.168.1.101/ekinerja/atasan.php"
//    var gantipw = "http://192.168.1.101/ekinerja/gantipw.php"

//    var urlshowprofile = "http://192.168.43.228/ekinerja/show_profil.php"
//    var urlatasan = "http://192.168.43.228/ekinerja/atasan.php"
//    var gantipw = "http://192.168.43.228/ekinerja/gantipw.php"

//    var urlshowprofile = "http://192.168.43.239/e-kinerja-web/show_profil.php"
//    var urlatasan = "http://192.168.43.239/e-kinerja-web/atasan.php"
//    var gantipw = "http://192.168.43.239/e-kinerja-web/gantipw.php"

//    URL WW
//    var urlshowprofile = "http://192.168.43.131/e-kinerja-web/show_profil.php"
//    var urlatasan = "http://192.168.43.131/e-kinerja-web/atasan.php"
//    var gantipw = "http://192.168.43.131/e-kinerja-web/gantipw.php"
    lateinit var session: SharedPref
    var _id = ""
    var password = ""
    var idatasan = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView((R.layout.activity_ubahpw))
        btnSimpanPw.setOnClickListener(this)
        session = SharedPref(applicationContext)
        _id = SharedPref.getInstance(applicationContext).user.nip.toString()
    }

    override fun onStart() {
        super.onStart()
        ShowProfile(_id)
    }

    fun ShowProfile(id: String) {
        val request = object : StringRequest(
            Request.Method.POST, urlshowprofile,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prof = HashMap<String, String>()
                    prof.put("nip", jsonObject.getString("nip"))
                    prof.put("password", jsonObject.getString("password"))
                    prof.put("nama", jsonObject.getString("nama"))
                    prof.put("golongan", jsonObject.getString("golongan"))
                    prof.put("jabatan", jsonObject.getString("jabatan"))
                    prof.put("eselon", jsonObject.getString("eselon"))
                    prof.put("unit_kerja", jsonObject.getString("unit_kerja"))
                    prof.put("id_atasan", jsonObject.getString("id_atasan"))

                    //textView57.setText(prof.getValue("nip").toString())
                    textView26.setText(prof.getValue("password").toString())
                    //textView58.setText(prof.getValue("nama").toString())
                    //textView56.setText(prof.getValue("golongan").toString())
                    //textView62.setText(prof.getValue("jabatan").toString())
                    //textView63.setText(prof.getValue("eselon").toString())
                    //textView64.setText(prof.getValue("unit_kerja").toString())
                    idatasan = prof.getValue("id_atasan").toString()
                    atasan(idatasan)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ) {
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String, String>()
                hm.put("_id", id)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(applicationContext)
        queue.add(request)
    }

    fun atasan(id: String) {
        val request = object : StringRequest(
            Request.Method.POST, urlatasan,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prof = HashMap<String, String>()
                    prof.put("nama", jsonObject.getString("nama"))
                    prof.put("jabatan", jsonObject.getString("jabatan"))

                    //textView60.setText(prof.getValue("nama").toString())
                    //textView65.setText(prof.getValue("jabatan").toString())
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, "gagal terhubung", Toast.LENGTH_SHORT).show()
            }
        ) {
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String, String>()
                hm.put("_id", id)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(applicationContext)
        queue.add(request)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSimpanPw -> {
                password(_id)
                password = pw_baru.text.toString()
                Toast.makeText(this, "Berhasil mengubah password $_id, $password", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun password(id: String) {
        val request = object : StringRequest(
            Request.Method.POST, gantipw,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                    val error = jsonObject.getString("kode")
                    if(error.equals("000")){
                        Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    }
                    else {
                        Toast.makeText(this, "Operasi Gagal", Toast.LENGTH_LONG).show()
                    }

                    //textView60.setText(prof.getValue("nama").toString())
                    //textView65.setText(prof.getValue("jabatan").toString())

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, "Gagal mengubah password", Toast.LENGTH_SHORT).show()
            }
        ) {
            override fun getParams(): MutableMap<String, String> {

                val hm = HashMap<String, String>()
                hm.put("_id", id)
                hm.put("password", password)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(applicationContext)
        queue.add(request)

    }
}